#!/usr/bin/perl

use 5.014;
use DBI;
use Asterisk::AMI;
use EV;

my $host='10.0.8.199';
my $database='servers';
my $user='abdi';
my $password='12345678';
my $ok = 1;
my $fail = 2;

my $dsn = "DBI:mysql:database=$database;host=$host;port=3306";
my $dbh = DBI->connect($dsn, $user, $password);

my $ary_ref  = $dbh->selectall_hashref('select * from servers_status', 'server_ip');

for my $ip (keys %{$ary_ref}) {

	my $ami = Asterisk::AMI->new(PeerAddr => $ip,
							 PeerPort => '5038',
							 Username => 'admin',
							 Secret => 'admin',
							 Events => 'on',
	);
	my $sth = $dbh->prepare("UPDATE servers_status set status=? WHERE server_ip='$ip'");
	$sth->execute ($ami ? $ok : $fail);
}
