#!/usr/bin/perl -w 

use strict;
use Switch;
use Getopt::Long;
use LWP::UserAgent;
use Authen::NTLM;
use HTTP::Request::Common;
use HTTP::Response;



my $url = '';
my $user = '';
my $pass = '';
my $realm = '';

my $res = GetOptions (
    'url=s'             => \$url,
    'secret=s'          => \$user,
    'password=s'        => \$pass,
    'realm=s'           => \$realm,
);

die("Usage: $0 -u [url] -s [secret] -p [pass]\n") unless ($url and $user and $pass);
print "$url, $user, $pass $realm\n";

sub basic_auth {
    my $ua = LWP::UserAgent->new;
    my $req = HTTP::Request->new(GET => $url);
    $req->authorization_basic($user,$pass);
    print $ua->request($req)->as_string;
    find_content($ua->request($req)->as_string, "your");
}

sub ntlm_auth {
    my $username="\\$user";
    my ($host)      = $url =~ m{http://([^/]*)/}i;

    my $ua = LWP::UserAgent->new( keep_alive => 1 );
    $ua->credentials( $host, '', $username, $pass);
    my $req = GET $url;

    my $response = $ua->request($req);
    if ( $response->is_error() ) {
        printf " %s\n", $response->status_line;
    }
    else {
        print $response->headers()->as_string(), "\n";
        print $response->content;
        find_content($response->content, "nowrap");
    }
}

sub digest_auth {
    my $ua = new LWP::UserAgent(keep_alive => 1);

    my ($host)      = $url =~ m{http://([^/]*)/}i;
    print "$user, $pass, $realm, $url, $host\n";
    $ua->credentials($host, $realm, $user, $pass);

    my $req = HTTP::Request->new(GET => $url); 
    my $res = $ua->request($req);

    if ($res->is_success) {
        print $res->content;
        find_content($res->content, "Yves");
    } else {
        print "Error: " . $res->status_line . "\n";
        print $res->headers()->as_string(), "\n";
    }
}

sub find_content {
    my ($content, $pat) = @_;
    my $res = '';

    ($res) = $content =~ /$pat(\s*[^\s.]*)/;
    print "result is: \"$res\"\n";
    return $res;
}

my $ua = LWP::UserAgent->new( keep_alive => 1 );
my $req = GET $url;
my $response = $ua->request($req);

print "method: ". $response->header("WWW-Authenticate"), "\n";

switch ($response->header("WWW-Authenticate")) {
    case /basic/i    { basic_auth(); }
    case /digest/i   { digest_auth(); }
    case /ntlm/i     { ntlm_auth(); }
    else             { print "nothing\n"; }
}
