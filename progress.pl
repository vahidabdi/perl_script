$| = 1;
my $var = 0;

while ($var <= 100) {
  sleep 1;
  printf "\rprogress %3d \% [%-50s]\r", $var, "#" x ($var/2);
  $var += 1;
}
print "\nFinished.\n";
