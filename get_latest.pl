#!/usr/bin/perl -w

use strict;
use warnings;
use 5.010;
use Asterisk::AGI;

my $agi = new Asterisk::AGI;

my $path = $ARGV[0] || "/home/vahid/scripts/files/";
my $range = 10;

my @files = ();

sub dir_walk {
    my ($top, $filefunc, $dirfunc) = @_;
    my $DIR;

    if (-d $top) {
        my $file;
        unless (opendir $DIR, $top) {
            warn "Could not open directory $top: $!; skipping.\n";
            return;
        }

        my @results;
        while ($file = readdir $DIR) {
            next if $file eq '.' || $file eq '..';
            push @results, dir_walk("$top/$file", $filefunc, $dirfunc);
        }
        return $dirfunc ? $dirfunc->($top, @results) : ();
     } else {
        return $filefunc ? $filefunc->($top) : ();
    }
}

sub get_files {

    my ($file) = @_;
    if ($file =~ m/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})$/) {
        push @files, "$1$2$3$4$5";
    }
}

dir_walk($path, \&get_files);

my @sorted_files = sort {$b cmp $a} @files;

say "latest 10 files are....";
if  ($#sorted_files < $range) {
    say shift @sorted_files;
    return;
} else {
    for my $i (1..10) {
        $agi->set_variable("latest_$i", shift @sorted_files);
    }
}

say "random files are....";
if  ($#sorted_files < $range) {
    say for @sorted_files;
    return;
} else {
    for my $i (1..10) {
        $agi->set_variable("random_$i", $sorted_files[int(rand($#sorted_files))]);
    }
}
