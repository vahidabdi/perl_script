#!/usr/bin/perl -w

use strict;
use 5.014;

sub dir_walk {
    my ($top, $filefunc, $dirfunc) = @_;
    my $DIR;

    if (-d $top) {
        my $file;
        unless (opendir $DIR, $top) {
            warn "Could not open directory $top: $!; skipping.\n";
            return;
        }

        my @results;
        while ($file = readdir $DIR) {
            next if $file eq '.' || $file eq '..';
            push @results, dir_walk("$top/$file", $filefunc, $dirfunc);
        }
        return $dirfunc ? $dirfunc->($top, @results) : ();
     } else {
        return $filefunc ? $filefunc->($top) : ();
    }
}
1;
