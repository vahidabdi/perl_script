#!/usr/bin/perl -w

use strict;
use 5.014;
use Asterisk::AMI;

my $ami = Asterisk::AMI->new(
	PeerAddr => '127.0.0.1',
	PeerPort =>	'5038',
	Username => 'admin',
	Secret => 'admin'
);
die "Could not connect to AMI." unless ($ami);

#my $res = $ami->action({Action => 'Command', command => 'core show channels concise'});
#my $res1 = $ami->send_action({Action => 'Getvar', Variable => 'last_week', Channel => 'SIP/200-00000000'});
my $res = $ami->action({Action => 'Getvar', Variable => 'last_week', Channel => 'SIP/200-00000000'});
#my $res = $ami->get_response($res1);
say $res;
for my $k (keys %$res) {
    say "\t$k ---> $res->{$k}";
    if ($k eq 'PARSED') {
        for my $k1 (keys %$res->{$k}) {
            say "\t\t$k1 ---> $res->{$k}{$k1}";
        }
    }
}
#my @ch = @{$res->{'CMD'}};

# SIP/200-00000009!from-internal!1000!3!Up!Wait!100!200!!!3!2!(None)!1375785386.9
#for (@ch) {
#	say;
#	my ($channel,$context,$exten,$pri,$state,$app,$data,
#		$callerid,$duration,$accountcode,$peeraccount,
#		$bridgedto,$id) = split(/!/, $_);
#	say "$app,$data" if defined ($ARGV[0]) && $channel =~ /$ARGV[0]/i;
#}

