#!/usr/bin/perl

use 5.014;
use DBI;
use Asterisk::AMI;

my $host='10.0.8.199';
my $database='servers';
my $user='abdi';
my $password='12345678';
my $ok = 1;
my $fail = 2;

my $dsn = "DBI:mysql:database=$database;host=$host;port=3306";
my $dbh = DBI->connect($dsn, $user, $password);

my $ary_ref  = $dbh->selectall_hashref('select * from servers_status', 'server_ip');

IP:
for my $ip (keys %{$ary_ref}) {
	say "testing $ip ...";

	my $ami = Asterisk::AMI->new(PeerAddr => $ip,
							 PeerPort => '5038',
							 Username => 'admin',
							 Secret => 'admin',
							 Events => 'on',
	);
	unless ($ami) {
		my $sth = $dbh->prepare("UPDATE servers_status set status=$fail,description='cant connect to ami' where server_ip='$ip'");
		$sth->execute();
		next IP;
	}


	say "Connected to ami.";

	my $res = $ami->action({Action => "Command",
	                        command => 'core show channels'
	});


	my $sth = $dbh->prepare("UPDATE servers_status set status=?,description='' where server_ip='$ip'");
	$sth->execute ($res->{'GOOD'} ? $ok : $fail);

}
