#! /usr/local/bin/perl

use strict;
use 5.010;
use Net::SNMP;

my $OID_lmTempSensorsValue =  '1.3.6.1.4.1.2021.13.16.2.1.3.1';

my $se = Net::SNMP->session(
    -hostname     => 'localhost',
    -community    => 'public',
);

my $cur = $se->get_request(
    -varbindlist =>  [$OID_lmTempSensorsValue ],
);


for my $key (keys $cur) {
    say "$key: $cur->{$key}";
}

