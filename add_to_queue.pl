#!/usr/bin/perl -w

use strict;

use Asterisk::AGI;
use Beanstalk::Client;

my $AGI = new Asterisk::AGI;
my $channel = $AGI->get_variable("CHANNEL");
my $conf = $AGI->get_variable("conference");

my $client = Beanstalk::Client->new(
  { server       => "localhost",
    default_tube => $conf,
  }
);

my $job = $client->put(
  { data     => "$channel",
  }
);

$AGI->exec("NoOp", "$job->{'id'}");
$AGI->exec("Set", "job_id=$job->{'id'}");

