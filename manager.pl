#!/usr/bin/perl -w

use 5.010;
use Asterisk::AMI;
use EV;
my $ami = Asterisk::AMI->new(PeerAddr => '127.0.0.1',
							 PeerPort => '5038',
							 Username => 'admin',
							 Secret => 'admin',
							 Events => 'on',
							 Handlers => { default => \&eventhandler,
                                           'CoreShowChannelsComplete' => \&ActiveChannels}
						);
die "Unable to connect to Asterisk." unless ($ami);	

sub eventhandler {
	my ($ami, $event) = @_;
	say 'Got Event: ', $event->{'Event'};
}

sub ActiveChannels {
	my ($ami, $event) = @_;
	say 'Active Channels: ', $event->{'ListItems'};
}

sub actioncb {
	my ($ami, $response) = @_;
	say "got action response: ", $response->{'Response'};
}

my $act = $ami->send_action({Action => "CoreShowChannels"}, \&actioncb);
EV::loop;