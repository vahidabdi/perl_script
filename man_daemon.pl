#!/usr/bin/perl -w

use strict;
use POSIX qw(setsid);
#use 5.010;
use EV;
use DBI;
use Asterisk::AMI;
no warnings qw(Asterisk::AMI);

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($ERROR);

my %conf_param = (
  "log4perl.rootLogger"                                 => "DEBUG, LOGFILE",
  "log4perl.appender.LOGFILE"                           => "Log::Log4perl::Appender::File",
  "log4perl.appender.LOGFILE.filename"                  => "/var/log/ast_daemon.log",
  "log4perl.appender.LOGFILE.mode"                      => "append",
  "log4perl.appender.LOGFILE.layout"                    => "PatternLayout",
  "log4perl.appender.LOGFILE.layout.ConversionPattern"  => "[%d] %c - %m%n"
);

Log::Log4perl->init( \%conf_param );

my $logger = Log::Log4perl->get_logger("Asterisk");

my $daemonized = 1;
my $pidfile = "/var/run/manager.pid";

if ( $daemonized == 1 ) {
  defined( my $pid = fork ) or die "Can't Fork: $!";
  exit if $pid;
  setsid or die "Can't start a new session: $!";
  open MYPIDFILE, ">$pidfile"
    or die "Failed to open PID file $pidfile for writing.";
  print MYPIDFILE $$;
  close MYPIDFILE;

  close STDIN;
  close STDOUT;
  close STDERR;
}

my $dbh = DBI->connect('DBI:mysql:ghese:host=10.2.29.199', 'abdi', '12345678')
  or die $logger->debug("Problem to connent ot the database: $DBI::errstr; ");
  #or die "Problem to connent ot the database: $DBI::errstr; ";

my $astman = Asterisk::AMI->new(PeerAddr => "127.0.0.1",
                                PeerPort => "5038",
                                Username => "admin",
                                Secret   => "admin",
                                Events   => "on",
                                Handlers => { ConfbridgeJoin  => \&do_join,
                                              ConfbridgeLeave => \&do_leave }
);
die "Unable to connect to asterisk" unless ($astman);

sub do_join {
  my ($asterisk, $event) = @_;
  $logger->debug("Got event: $event->{'Event'}");
  for my $key (keys %$event) {
    $logger->debug("---> $key: $event->{$key}");
  }

  my $sql = "SELECT id FROM conference where name=?";
  my $sth = $dbh->prepare($sql);
  $sth->execute($event->{'Conference'});
  my $conf_id = $sth->fetchrow();

  $sql = "INSERT INTO conference_online(conf_id, callerid, uniqueid,channel) VALUES(?, ?, ?, ?)";
  $sth = $dbh->prepare($sql);
  $sth->execute(($conf_id, $event->{'CallerIDnum'}, $event->{'Uniqueid'}, $event->{'Channel'}));
  $sth = $dbh->prepare("SELECT id from conference_archive where uniqueid=?");
  $sth->execute($event->{'Uniqueid'});
  my $archive_id = $sth->fetchrow();
  $sql = "INSERT INTO conference_log(confrence_id, call_archive_id) VALUES(?, ?)";
  $sth = $dbh->prepare($sql);
  $sth->execute(($event->{'Conference'}, $archive_id));
  my $id = $sth->{mysql_insertid};

  $astman->send_action({ Action    => "Setvar",
                         Variable  => "join_id",
                         Channel   => $event->{'Channel'},
                         Value     => $id });

}


sub do_leave {

  my ($asterisk, $event) = @_;
  my $join_id;
  my $id = $astman->send_action({ Action    => "Getvar",
                                  Variable  => "join_id",
                                  Channel   => $event->{'Channel'},
                                }, \&get_value, undef, \$join_id);

  $logger->debug("Got event: $event->{'Event'}");
  for my $key (keys %$event) {
    $logger->debug("---> $key: $event->{$key}");
  }


  my $sql = "DELETE FROM conference_online where uniqueid=?";
  my $sth = $dbh->prepare($sql);
  $sth->execute($event->{'Uniqueid'});
}

sub get_value {
  my ($ast, $resp, $ref_join_id) = @_;
  for my $key (keys %$resp) {
    if ($key eq "PARSED") {
      $$ref_join_id = $resp->{$key}{"Value"};
      my $sql = "UPDATE conference_log SET call_duration=(TIMESTAMPDIFF(SECOND, (SELECT conf_start), NOW())), conf_end=(NOW()) WHERE id=$$ref_join_id";
      my $sth = $dbh->prepare($sql);
      $sth->execute();
    }
  }
}

EV::loop
