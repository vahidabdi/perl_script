#!/usr/bin/perl -w

use strict;

use Asterisk::AGI;

my $AGI = new Asterisk::AGI;
my $job_id = $AGI->get_variable("job_id");

if ($job_id) {
  $AGI->exec("Set", "muteme=$job_id");
}
