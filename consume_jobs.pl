#!/usr/bin/perl

use strict;
use POSIX qw(setsid);

use Parallel::ForkManager;
use AnyEvent;
use EV;

use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($ERROR);

use Asterisk::AMI;
use Beanstalk::Client;

use constant TIMEOUT => 40;

my %conf_param = (
  "log4perl.rootLogger"                                 => "DEBUG, LOGFILE",
  "log4perl.appender.LOGFILE"                           => "Log::Log4perl::Appender::File",
  "log4perl.appender.LOGFILE.filename"                  => "/var/log/conference_queue.log",
  "log4perl.appender.LOGFILE.mode"                      => "append",
  "log4perl.appender.LOGFILE.layout"                    => "PatternLayout",
  "log4perl.appender.LOGFILE.layout.ConversionPattern"  => "[%d] %c - %m%n"
);

Log::Log4perl->init( \%conf_param );

my $logger = Log::Log4perl->get_logger("Conference Queue");

my $daemonized = 1;
my $pidfile = "/var/run/conf_queue.pid";

if ( $daemonized == 1 ) {
  defined( my $pid = fork ) or die $logger->debug("Can't Fork: $!");
  exit if $pid;
  setsid or die $logger->debug("Can't start a new session: $!");
  open MYPIDFILE, ">$pidfile"
    or die $logger->debug("Failed to open PID file $pidfile for writing.");
  print MYPIDFILE $$;
  close MYPIDFILE;

  close STDIN;
  close STDOUT;
  close STDERR;
}



$logger->debug("PARENT PID $$");

my @tubes = qw { queue1 queue };

my $pm = Parallel::ForkManager->new(scalar @tubes);

while (1) {
  for my $tube (@tubes) {
    my $pid = $pm->start and next;

    my $client = Beanstalk::Client->new(
      { server       => "localhost",
        default_tube => $tube,
      }
    );

    process_child($tube, $client);
    $pm->finish;
  }
}

sub process_child {

  my ($tube, $client) = @_;
  my $job = undef;
  JOB: while (1) {
    $logger->debug("$$ -> Waiting to get a new job on $tube");
    $job = $client->reserve;
    $logger->debug("$$ -> Let's process $job->{'data'} in $tube");

    if ($job) {
      my $astman = Asterisk::AMI->new(PeerAddr  =>  '127.0.0.1',
                                      PeerPort  =>  '5038',
                                      Username  =>  'admin',
                                      Secret    =>  'admin',
                                      Events    =>  'on',
                                      Handlers  =>  { ConfbridgeLeave => \&do_leave,
                                                      VarSet          => \&var_set });
      die $logger->debug("$$ -> Unable to connect to asterisk") unless ($astman);

      my $cmd = `asterisk -rx "confbridge unmute $tube $job->{'data'}"`;
      if ($cmd =~ /^No (conference|channel).*/) {
        $logger->debug("$$ -> There's no $1");
        $job->delete;
        $logger->debug("$$ -> job $job->{'id'} deleted");
        sleep 1;
        next JOB;
      }

      my $timer = AnyEvent->timer (after => TIMEOUT, cb => \&do_timeout);

      sub do_timeout {
        $logger->debug("$$ -> Timeout: $job->{'data'} $job->{'id'}");
        system("asterisk -rx \"confbridge mute $tube $job->{'data'}\"");
        $job->delete;
        $logger->debug("$$ -> job $job->{'id'} deleted");
        sleep 1;
        EV::unloop;
      }

      sub var_set {
        my ($asterisk, $event) = @_;

        if ($event->{'Variable'} eq "muteme") {
          $logger->debug("muteme value is $event->{'Value'}");
          if ($job->{'id'} eq $event->{'Value'}) {
            $logger->debug("$job->{'data'} is speaking, we mute it now");
            system("asterisk -rx \"confbridge mute $tube $job->{'data'}\"");
            $job->delete;
            $logger->debug("$$ -> job $job->{'id'} deleted");
            EV::unloop;
          } else {
            $logger->debug("$job->{'data'} is NOT speaking, we remove it from $tube");
            my $n_job = $client->peek($event->{'Value'});
            $n_job->delete if $n_job;
          }
        }
      }

      sub do_leave {
        my ($asterisk, $event) = @_;
        if ($job->{'data'} eq $event->{'Channel'} && $tube eq $event->{'Conference'}) {
          $logger->debug("$$ -> $job->{'data'} eq $event->{'Channel'} && $tube eq $event->{'Conference'}");
          $logger->debug("$$ -> $event->{'Channel'} leaves conference $event->{'Conference'}");
          system("asterisk -rx \"confbridge mute $tube $job->{'data'}\"");
          $job->delete;
          $logger->debug("$$ -> job $job->{'id'} deleted");
          sleep 1;
          EV::unloop;
        } else {
          $logger->debug("$$ -> $job->{'data'} !eq $event->{'Channel'} || $tube !eq $event->{'Conference'} so who cares");
        }
      }

      EV::loop
    }
  }
}
